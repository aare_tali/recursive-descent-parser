package rdp;

class Pointer<T>
{
	private T value;

	public Pointer()
	{
		value = null;
	}

	Pointer(T value)
	{
		this.value = value;
	}

	void set(T value)
	{
		this.value = value;
	}

	T value()
	{
		return value;
	}
}
