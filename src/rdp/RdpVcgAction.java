package rdp;

import static rdp.GraphBase.*;
import static rdp.Scan.*;
import static rdp.Text.*;

class RdpVcgAction extends VcgAction
{
	private static int edge_count = 0;

	@Override
	public void edgeAction(GraphEdge base_edge)
	{
		RdpTreeEdgeData edge = (RdpTreeEdgeData) base_edge;
		/* Set user inserted edges to red */
		if (edge.rdp_edge_kind == 0)
		{
			text_printf(" color:red");
		}
		// TODO text_printf(" label:\"" + graph_atom_number(edge) + "\"");
		long order = 1000 - ++edge_count;
		if (order < 0)
		{
			order += 0x100000000L;
		}
		text_printf(" horizontal_order:" + order);
	}

	@Override
	public void nodeAction(GraphNode base_node)
	{
		RdpTreeNodeData node = (RdpTreeNodeData) base_node;
		// TODO space
		text_printf("label:\"");
		// TODO text_printf(graph_atom_number(node) + ": ");
		switch (node.data.token)
		{
		case SCAN_P_IGNORE:
		case SCAN_P_ID:
			text_print_C_string(text_get_string(node.data.id));
			break;
		case SCAN_P_INTEGER:
			text_printf("INTEGER: " + node.data.i);
			break;
		case SCAN_P_REAL:
			text_printf("REAL: " + node.data.r);
			break;
		case SCAN_P_STRING:
		case SCAN_P_STRING_ESC:
		case SCAN_P_COMMENT:
		case SCAN_P_COMMENT_VISIBLE:
		case SCAN_P_COMMENT_NEST:
		case SCAN_P_COMMENT_NEST_VISIBLE:
		case SCAN_P_COMMENT_LINE:
		case SCAN_P_COMMENT_LINE_VISIBLE:
			text_printf("\\\"");
			text_print_C_string(text_get_string(node.data.id));
			text_printf("\\\"");
			break;
		case SCAN_P_EOF:
			text_printf("EOF");
			break;
		case SCAN_P_EOLN:
			text_printf("EOLN");
			break;
		default:
			text_printf("\'");
			text_print_C_string(text_get_string(node.data.id));
			text_printf("\'");
			break;
		}
		text_printf("\" horizontal_order:" + graph_atom_number(node));

		if (node.data.token == SCAN_P_IGNORE)
		{
			text_printf(" shape:ellipse ");
		}
	}
}
