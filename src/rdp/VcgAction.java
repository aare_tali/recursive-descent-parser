package rdp;

import static rdp.Text.*;

abstract class VcgAction
{
	void edgeAction(GraphEdge edge)
	{
	}

	void graphAction(Graph<?, ?> graph)
	{
	}

	void nodeAction(GraphNode node)
	{
		text_printf("label: \"Node:" + node.atom_number + "\"");
	}
}
