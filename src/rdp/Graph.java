package rdp;

import static rdp.GraphNode.*;
import static rdp.Text.*;

class Graph<NodeData extends GraphNode, EdgeData extends GraphEdge>
{
	/** The number of the next graph to be created */
	private static int graph_next_graph_count = 1;

	/** The number of the next edge to be created */
	static int graph_next_edge_count = 1;

	/** The list of active graph structures */
	static Graph<? extends GraphNode, ? extends GraphEdge> graph_list = new Graph<GraphNode, GraphEdge>();

	static void graph_vcg(Graph<?, ?> graph, VcgAction action)
	{
		text_printf("graph:{\n" + "orientation:top_to_bottom\n" + "edge.arrowsize:7\n" + "edge.thickness:1\n"
				+ "display_edge_labels:yes\n" + "arrowmode:free\n" + "node.borderwidth:1\n");

		if (graph == null)
		{
			/* dump all graphs */
			Graph<?, ?> curr_graph = graph_list.next_graph;
			while (curr_graph != null)
			{
				curr_graph.vcgGraph(action);
				curr_graph = curr_graph.next_graph;
			}
		}
		else
		{
			/* dump specific graph */
			graph.vcgGraph(action);
		}
		text_printf("\n}\n");
	}

	GraphNode root;
	Graph<? extends GraphNode, ? extends GraphEdge> next_graph;
	Graph<? extends GraphNode, ? extends GraphEdge> previous_graph;
	GraphNode next_node;
	String id;
	int atom_number;

	void insertGraph(String id)
	{
		atom_number = graph_next_graph_count++;
		next_node = null;
		graph_next_node_count = graph_next_edge_count = 1;
		// Now insert at destination of graph_list
		next_graph = graph_list.next_graph;
		graph_list.next_graph = this;
		previous_graph = graph_list;
		// if rest of list is non-null... point next node back at us
		if (next_graph != null)
		{
			next_graph.previous_graph = this;
		}
		this.id = id;
	}

	void insertNode(GraphNode node)
	{
		node.atom_number = graph_next_node_count++;
		node.next_out_edge = null;
		/* Now insert after node_or_graph */
		/* look at rest of list */
		node.next_node = next_node;
		/* point previous at this node */
		next_node = node;
		/* point backlink at base pointer */
		node.previous_node = null;
		/* point next node back at us */
		if (node.next_node != null)
		{
			node.next_node.previous_node = node;
		}
	}

	void setRoot(GraphNode root)
	{
		this.root = root;
	}

	private void vcgGraph(VcgAction action)
	{
		action.graphAction(this);
		GraphNode curr_node = next_node;
		while (curr_node != null)
		{
			text_printf("node:{title:\"" + curr_node.atom_number + "\"");
			action.nodeAction(curr_node);
			text_printf("}\n");
			GraphEdge curr_edge = curr_node.next_out_edge;
			while (curr_edge != null)
			{
				text_printf("edge:{sourcename:\"" + curr_node.atom_number + "\" targetname:\""
						+ curr_edge.destination.atom_number + "\"");
				action.edgeAction(curr_edge);
				text_printf("}\n");
				curr_edge = curr_edge.next_out_edge;
			}
			curr_node = curr_node.next_node;
		}
	}
}
