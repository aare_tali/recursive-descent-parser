package rdp;

import static rdp.RdpAux.*;
import static rdp.Text.*;
import static rdp.Text.TextMessageType.*;

class RdpData extends Symbol
{
	int token;
	/** token value for tokens */
	int token_value;
	/** extended value for tokens */
	int extended_value;
	int kind;
	/** return_type name */
	String return_type;
	/** number of indirections in return type */
	int return_type_stars;
	/** pointer to token value as a string */
	int token_string;
	/** pointer to token value as enum element */
	int token_enum;
	/** pointer to extended value as enum element */
	String extended_enum;
	/** default promotion operator */
	int promote_default;
	/** promotion operator for inline calls */
	int promote;
	/** promotion operator for iterator delimiters */
	int delimiter_promote;
	/**
	 * flag to suppress unused production warning if production contains only
	 * comments
	 */
	boolean comment_only;
	/** for quick first calculation */
	boolean contains_null;
	/** production has inherited attributes */
	boolean parameterised;
	/** mark items that follow a code item */
	int code_successor;
	/** mark last code item in sequence */
	int code_terminator;
	/** primary production with code only */
	int code_only;
	/** has appeared on LHS of ::= */
	int been_defined;
	/** production being checked flag */
	int in_use;
	/** ll(1) violation detected */
	int ll1_violation;
	/** first() completed on this production */
	int first_done;
	/** follow() completed on this production */
	int follow_done;
	/** set of first symbols */
	final Set first = new Set();
	/** how many times production is called */
	int call_count;
	/** number of elements in first set */
	int first_cardinality;
	/** set of follow symbols */
	final Set follow = new Set();
	/** number of elements in follow set */
	int follow_cardinality;
	/** active parser pass for code element */
	int code_pass;
	/** minimum iteration count */
	int lo;
	/** maximum iteration count */
	int hi;
	/** list of parameter names (and types) */
	RdpParamList params;
	/** list of actuals filled in by item_ret */
	RdpParamList actuals;
	/** list of alternatives or items */
	RdpList list;
	/** spare token pointer */
	RdpData supplementary_token;
	/** extended keyword close string */
	String close;

	void rdp_print_sub_item(boolean expand)
	{
		switch (kind)
		{
		case K_INTEGER:
		case K_STRING:
		case K_REAL:
		case K_EXTENDED:
			text_printf(text_get_string(id) + " ");
			break;
		case K_TOKEN:
			text_printf("\'" + text_get_string(id) + "\' ");
			break;
		case K_CODE:
			/* Don't print anything */
			break;
		case K_PRIMARY:
			text_printf(text_get_string(id) + " ");
			break;
		case K_SEQUENCE:
			rdp_print_sub_sequence(expand);
			break;
		case K_LIST:
			if (expand)
			{
				/* first find special cases */
				/* All EBNF forms have no delimiter */
				if (supplementary_token == null)
				{
					if (lo == 0 && hi == 0)
					{
						text_printf("{ ");
						rdp_print_sub_alternate(expand);
						text_printf("} ");
					}
					else if (lo == 0 && hi == 1)
					{
						text_printf("[ ");
						rdp_print_sub_alternate(expand);
						text_printf("] ");
					}
					else if (lo == 1 && hi == 0)
					{
						text_printf("< ");
						rdp_print_sub_alternate(expand);
						text_printf("> ");
					}
					else if (lo == 1 && hi == 1)
					{
						text_printf("( ");
						rdp_print_sub_alternate(expand);
						text_printf(") ");
					}
				}
				else
				{ /* Now do general case */
					text_printf("( ");
					rdp_print_sub_alternate(expand);
					text_printf(")" + lo + "@" + hi);
					if (supplementary_token != null)
					{
						text_printf(" \'" + text_get_string(supplementary_token.id) + "\'");
					}
					else
					{
						text_printf(" #");
					}
				}
			}
			else
			{
				text_printf(text_get_string(id) + " ");
			}
			break;
		default:
			text_message(TEXT_FATAL, "internal error - unexpected kind found\n");
		}
	}

	private void rdp_print_sub_alternate(boolean expand)
	{
		RdpList list = this.list;
		while (list != null)
		{
			list.production.rdp_print_sub_item(expand);
			if ((list = list.next) != null)
			{
				text_printf("| ");
			}
		}
	}

	private void rdp_print_sub_sequence(boolean expand)
	{
		RdpList list = this.list;
		while (list != null)
		{
			list.production.rdp_print_sub_item(expand);
			list = list.next;
		}
	}
}
