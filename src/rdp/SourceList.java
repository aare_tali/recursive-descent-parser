package rdp;

import java.io.*;

class SourceList
{
	/** copy of filename */
	String name;

	/** copy of total errors for this file */
	int errors;

	/** copy of current file handle */
	InputStream file;

	/** copy of first character of current source line */
	int first_char;

	/** copy of last character of current source line */
	int last_char;

	/** copy of current line in this file */
	int linenumber;

	/** copy of current text character */
	int text_char;

	/** copy of pointer to current source character */
	int text_current;

	/** copy of pointer to the last thing read by the scanner */
	ScanData text_scan_data;

	/** copy of first character of this symbol */
	int symbol_first_char;

	/** copy of total warnings for this file */
	int warnings;

	/** previous file descriptor */
	SourceList previous;
}
