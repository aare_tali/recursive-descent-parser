package rdp;

import static rdp.Text.*;

abstract class Symbol implements Comparable<Symbol>
{
	/** next symbol in hash list */
	Symbol next_hash;
	/** pointer to next pointer of last_symbol in hash list */
	final Pointer<Symbol> last_hash = new Pointer<Symbol>();

	/** next symbol in scope list */
	Symbol next_scope;

	/** pointer to the scope symbol */
	Symbol scope;

	/** hash value for quick searching */
	int hash;

	int id;

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Symbol other)
	{
		return text_get_string(id).compareTo(text_get_string(other.id));
	}

	protected void unlinkSymbol()
	{
		Symbol s = this;

		s.last_hash.set(s.next_hash); /* point previous pointer to next symbol */
		if (s.next_hash != null)
		{
			s.next_hash.last_hash.set(s.last_hash.value());
		}
	}

	void memset()
	{
		hash = 0;
		id = 0;
		last_hash.set(null);
		next_hash = null;
		next_scope = null;
		scope = null;
	}

	/** Return next symbol in scope chain. Return NULL if at end */
	Symbol nextSymbolInScope()
	{
		return next_scope;
	}

	void print()
	{
		text_printf(id == 0 ? "Null symbol" : text_get_string(id));
	}
}
