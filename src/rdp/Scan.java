package rdp;

import static rdp.Scanner.*;
import static rdp.Set.*;
import static rdp.SymbolTable.*;
import static rdp.Text.*;
import static rdp.Text.TextMessageType.*;

class Scan
{
	static final int SCAN_P_IGNORE = 0;
	static final int SCAN_P_ID = 1;
	static final int SCAN_P_INTEGER = 2;
	static final int SCAN_P_REAL = 3;
	static final int SCAN_P_CHAR = 4;
	static final int SCAN_P_CHAR_ESC = 5;
	static final int SCAN_P_STRING = 6;
	static final int SCAN_P_STRING_ESC = 7;
	static final int SCAN_P_COMMENT = 8;
	static final int SCAN_P_COMMENT_VISIBLE = 9;
	static final int SCAN_P_COMMENT_NEST = 10;
	static final int SCAN_P_COMMENT_NEST_VISIBLE = 11;
	static final int SCAN_P_COMMENT_LINE = 12;
	static final int SCAN_P_COMMENT_LINE_VISIBLE = 13;
	static final int SCAN_P_EOF = 14;
	static final int SCAN_P_EOLN = 15;
	static final int SCAN_P_TOP = 16;

	static boolean scan_case_insensitive = false;
	static boolean scan_newline_visible = false;
	private static boolean scan_show_skips = false;
	static boolean scan_symbol_echo = false;
	private static String[] scan_token_names = null;
	static SymbolTable scan_table = null;
	static boolean scan_lexicalise_flag = false;

	static void scan_init(boolean case_insensitive, boolean newline_visible, boolean show_skips, boolean symbol_echo,
			String[] token_names)
	{
		scan_case_insensitive = case_insensitive;
		scan_show_skips = show_skips;
		scan_newline_visible = newline_visible;
		scan_symbol_echo = symbol_echo;
		scan_token_names = token_names;

		scan_comment_list = new ScanCommentBlock();
		scan_comment_list_end = scan_comment_list;
		text_scan_data = new ScanData();
		scan_table = symbol_new_table("scan table", 101, 31, new CompareHashPrint());
		scan_insert_comment_block("", 0, 0);
	}

	static void scan_lexicalise()
	{
		scan_lexicalise_flag = true;
	}

	static void scan_load_keyword(String id1, String id2, int token, int extended)
	{
		ScanData d = new ScanData();
		d.id = text_insert_string(id1);
		if (id2 != null)
		{
			text_insert_string(id2);
		}
		d.token = token;
		d.extended = extended;
		scan_table.insert(d);
	}

	static boolean scan_test(String production, int valid, Set stop)
	{
		if (valid != text_scan_data.token)
		{
			if (stop != null)
			{
				printScannedToken(production);
				text_printf(" while expecting ");
				set_print_element(valid, scan_token_names);
				text_printf("\n");
				skip(stop);
			}
			return false;
		}
		return true;
	}

	static boolean scan_test_set(String production, Set valid, Set stop)
	{
		if (!valid.includes(text_scan_data.token))
		{
			if (stop != null)
			{
				printScannedToken(production);
				text_printf(" while expecting " + (set_cardinality(valid) == 1 ? "" : "one of "));
				valid.print(scan_token_names, 60);
				text_printf("\n");
				skip(stop);
			}
			return false;
		}
		return true;
	}

	private static void printScannedToken(String production)
	{
		if (production != null)
		{
			text_message(TEXT_ERROR_ECHO, "In rule \'" + production + "\', scanned ");
		}
		else
		{
			text_message(TEXT_ERROR_ECHO, "Scanned ");
		}
		set_print_element(text_scan_data.token, scan_token_names);
	}

	private static void skip(Set stop)
	{
		while (!stop.includes(text_scan_data.token))
		{
			scan_();
		}
		if (scan_show_skips)
		{
			text_message(TEXT_ERROR_ECHO, "Skipping to...\n");
		}
	}
}
