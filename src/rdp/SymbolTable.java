package rdp;

import static rdp.Text.*;
import static rdp.Text.TextMessageType.*;

class SymbolTable
{
	private static SymbolTable symbol_tables = null;

	static SymbolTable symbol_new_table(String name, int symbol_hashsize, int symbol_hashprime,
			CompareHashPrint compareHashPrint)
	{
		SymbolTable temp = new SymbolTable();
		SymbolScopeData scope = new SymbolScopeData();
		scope.id = text_insert_string("Global");
		temp.name = name;
		temp.hash_size = symbol_hashsize;
		temp.hash_prime = symbol_hashprime;
		temp.compareHashPrint = compareHashPrint;
		temp.table = new Symbol[symbol_hashsize];
		temp.current = temp.scopes = scope;

		// now hook into list of tables
		temp.next = symbol_tables;
		symbol_tables = temp;

		return temp;
	}

	/** diagnostic dump of all symbol tables */
	static void symbol_print_all_table()
	{
		SymbolTable temp = symbol_tables;
		while (temp != null)
		{
			temp.printTable();
			temp = temp.next;
		}
	}

	/** dump all bucket usage histograms */
	static void symbol_print_all_table_statistics(int histogram_size)
	{
		SymbolTable temp = symbol_tables;
		while (temp != null)
		{
			temp.printTableStatistics(histogram_size);
			temp = temp.next;
		}
	}

	/** an identifying string */
	String name;

	/** table of pointers to hash lists */
	Symbol[] table;

	/** pointers to chain of scope lists */
	SymbolScopeData current;

	/** pointer to first scope list */
	SymbolScopeData scopes;

	/** number of buckets in symbol table */
	int hash_size;
	/** hashing prime: hashsize and hashprime should be coprime */
	int hash_prime;
	CompareHashPrint compareHashPrint;

	/** pointer to last declared symbol table */
	SymbolTable next;

	int compare(String key, Symbol p)
	{
		return compareHashPrint.compare(key, p);
	}

	/** return current scope */
	SymbolScopeData getScope()
	{
		return current;
	}

	int hash(int prime, String key)
	{
		return compareHashPrint.hash(prime, key);
	}

	/** insert a symbol at head of hash list */
	Symbol insert(Symbol symbol)
	{
		Symbol s = symbol;
		s.hash = hash(hash_prime, text_get_string(symbol.id));
		int hash_index = s.hash % hash_size;
		s.next_hash = table[hash_index];
		table[hash_index] = s;
		s.last_hash.set(table[hash_index]);
		/* if this wasn't the start of a new list ... */
		if (s.next_hash != null)
		{
			/* ...point old list next back at s */
			s.next_hash.last_hash.set(s.next_hash);
		}
		/* now insert in scope list */
		s.next_scope = current.next_scope;
		current.next_scope = s;
		/* set up pointer to scope block */
		s.scope = current;
		return symbol;
	}

	/** lookup a symbol by id. Return null is not found */
	Symbol lookup(String key, SymbolScopeData scope)
	{
		int hash = hash(hash_prime, key);
		Symbol p = table[hash % hash_size];
		// look for symbol with same hash and a true compare
		while (!(p == null || compare(key, p) == 0 && !(p.scope != scope && scope != null)))
		{
			p = p.next_hash;
		}
		// Not found at all
		if (p == null)
		{
			return null;
		}
		return p;
	}

	SymbolScopeData newScope(String id)
	{
		SymbolScopeData p = new SymbolScopeData();
		p.id = text_insert_string(id);
		p.next_hash = scopes;
		current = scopes = p;
		if (p.next_hash != null)
		{
			p.next_hash.last_hash.set(p.next_hash);
		}
		return p;
	}

	void print(Symbol sym)
	{
		compareHashPrint.print(sym);
	}

	void printScope(Symbol sym)
	{
		Symbol this_scope = sym;
		sym = sym.nextSymbolInScope();
		text_printf("*** Start scope '" + text_get_string(this_scope.id) + "'\n");
		while (sym != null)
		{
			print(sym);
			text_printf("\n");
			sym = sym.nextSymbolInScope();
		}
		text_printf("*** End scope '" + text_get_string(this_scope.id) + "'\n");
	}

	private void printTable()
	{
		text_message(TEXT_INFO, "Symbol table \'" + name + "\': dump by hash bucket\n\n");
		for (int temp = 0; temp < hash_size; temp++)
		{
			text_printf("*** Bucket " + temp + ":\n");
			Symbol p = table[temp];
			while (p != null)
			{
				print(p);
				text_printf("\n");
				p = p.next_hash;
			}
		}
		text_printf("\n");
		text_message(TEXT_INFO, "Symbol table \'" + name + "\' dump by scope chain\n");
		Symbol p = scopes;
		while (p != null)
		{
			printScope(p);
			p = p.next_hash;
		}
		text_printf("\n");
	}

	private void printTableStatistics(int histogram_size)
	{
		int symbols = 0;
		int[] histogram = new int[histogram_size];
		for (int temp_unsigned = 0; temp_unsigned < hash_size; temp_unsigned++)
		{
			Symbol p = table[temp_unsigned];
			int count = 0;

			while (p != null)
			{
				symbols++;
				count++;
				p = p.next_hash;
			}
			/* bump histogram bucket */
			histogram[count < histogram_size - 1 ? count : histogram_size - 1]++;
		}
		text_message(TEXT_INFO, "\nSymbol table `" + name + "\' has " + hash_size + " buckets and contains " + symbols
				+ " symbols\n");
		/* Now calculate mean utilisation */
		int mean = 0;
		for (int temp_unsigned = 0; temp_unsigned < histogram_size; temp_unsigned++)
		{
			text_printf(histogram[temp_unsigned] + " bucket" + (histogram[temp_unsigned] == 1 ? "  has " : "s have")
					+ " " + temp_unsigned + " symbol"
					+ (temp_unsigned == 1 ? "" : temp_unsigned == histogram_size - 1 ? "s or more" : "s") + "\n");
			mean += (temp_unsigned + 1) * histogram[temp_unsigned];
		}
		text_message(TEXT_INFO, "\nMean list length is " + ((float) mean / (float) hash_size - 1) + "\n");
	}
}
