package rdp;

import static rdp.CRT.*;
import static rdp.RDP.*;
import static rdp.Scan.*;
import static rdp.Text.*;
import static rdp.Text.TextMessageType.*;

class Scanner
{
	private static boolean retain_comments = false;

	private static int scan_sequence_running_number = 0;

	@SuppressWarnings("unused")
	private static int last_line_number = 0;
	private static int last_column = 0;

	static ScanCommentBlock scan_comment_list = null;

	private static ScanCommentBlock last_comment_block;

	static ScanCommentBlock scan_comment_list_end = null;

	static void scan_()
	{
		int start;
		ScanData s;
		boolean nestable = false;
		int nestlevel = 0;
		int close;
		int last = ' ';
		do
		{
			start = text_top;
			text_scan_data.memset();
			// Don't do extendeds for non scanner table items
			text_scan_data.extended = SCAN_P_IGNORE;
			while (text_char != EOF && !(scan_newline_visible && text_char == '\n') && isspace(text_char))
			{
				if (scan_lexicalise_flag && text_char == '\n')
				{
					text_printf("\n");
				}
				text_get_char();
			}
			// Non zero means a token was restored at EOF
			if (text_scan_data.token != 0)
			{
				break;
			}
			last_column = text_column_number();
			last_line_number = text_line_number();
			if (isalpha(text_char) || text_char == '_')
			{
				/* read an identifier into text buffer */
				@SuppressWarnings("unused")
				int first_char = text_char;
				text_scan_data.id = text_top; /* point to text table */
				if (scan_case_insensitive && text_char >= 'A' && text_char <= 'Z')
				{
					text_char -= 'A' - 'a';
				}
				text_insert_char((char) text_char);
				text_get_char();
				while (isalnum(text_char) || text_char == '_')
				{
					if (scan_case_insensitive && text_char >= 'A' && text_char <= 'Z')
					{
						text_char -= 'A' - 'a';
					}
					text_insert_char((char) text_char);
					text_get_char();
				}
				text_insert_char('\0');
				if ((s = (ScanData) scan_table.lookup(text_get_string(text_scan_data.id), null)) != null)
				{
					memcpy(text_scan_data, s);
					text_top = start;
				}
				else
				{
					text_scan_data.token = SCAN_P_ID;
				}
			} /* end of ID collection */
			else if (isdigit(text_char))
			{
				// read a number of some sort
				boolean hex = false;
				// remember start position
				text_scan_data.id = text_top;
				// assume integer
				text_scan_data.token = SCAN_P_INTEGER;
				// Check for hexadecimal introducer
				if (text_char == '0')
				{
					text_insert_char((char) text_char);
					text_get_char();
					if (text_char == 'x' || text_char == 'X')
					{
						hex = true;
						text_insert_char((char) text_char);
						text_get_char();
					}
				}
				// Now collect decimal or hex digits
				while ((hex ? isxdigit(text_char) : isdigit(text_char)) || text_char == '_')
				{
					// suppress underscores
					if (text_char != '_')
					{
						text_insert_char((char) text_char);
					}
					text_get_char();
				}
				// check for decimal part and exponent
				if (!hex)
				{
					// get decimal with lookahead
					if (text_char == '.' && isdigit(text_bot[text_current - 1]))
					{
						text_scan_data.token = SCAN_P_REAL;
						do
						{
							text_insert_char((char) text_char);
							text_get_char();
						} while (isdigit(text_char));
					}
					// get exponent
					if (text_char == 'E' || text_char == 'e')
					{
						text_scan_data.token = SCAN_P_REAL;
						text_insert_char((char) text_char);
						text_get_char();
						if (text_char == '+' || text_char == '-' || isdigit(text_char))
						{
							do
							{
								text_insert_char((char) text_char);
								text_get_char();
							} while (isdigit(text_char));
						}
					}
				}
				// Now absorb any letters that are attached to the number
				while (isalpha(text_char))
				{
					text_insert_char((char) text_char);
					text_get_char();
				}
				text_insert_char('\0');
				if (text_scan_data.token == SCAN_P_INTEGER)
				{
					text_scan_data.i = Integer.parseInt(text_make_string(text_scan_data.id));
				}
				else
				{
					text_scan_data.r = Double.parseDouble(text_make_string(text_scan_data.id));
				}
			} /* end of number collection */
			else
			{
				// process non-alphanumeric symbol
				if (text_char == EOF)
				{
					text_scan_data.token = SCAN_P_EOF;
					text_scan_data.id = text_insert_string("EOF");
					text_top = start; /* scrub from text buffer */
					if (retain_comments)
					{
						scan_insert_comment_block("", 0, Integer.MAX_VALUE);
					}
				}
				else if (text_char == '\n')
				{
					text_top = start; /* scrub from text buffer */
					text_scan_data.token = SCAN_P_EOLN;
					text_scan_data.id = text_insert_string("EOLN");
					text_get_char();
				}
				else
				{
					start = text_top;
					ScanData last_sym;
					ScanData this_sym = null;
					for (;;)
					{
						last_sym = this_sym;
						text_insert_char((char) text_char);
						text_bot[text_top] = '\0';
						this_sym = (ScanData) scan_table.lookup(text_get_string(start), null);
						if (this_sym == null)
						{
							break;
						}
						text_get_char(); // collect longest match
					}
					// single character means mismatch
					if (text_top == start + 1)
					{
						char ch = text_bot[text_top - 1];
						text_message(TEXT_ERROR_ECHO, "Unexpected character 0x" + Integer.toHexString(ch) + " \'"
								+ (isprint(ch) ? ch : ' ') + "\' in source file\n");
						text_top = start; /* scrub from text buffer */
						text_scan_data.token = SCAN_P_IGNORE;
						text_get_char();
					}
					else
					{
						memcpy(text_scan_data, last_sym);
					}
					text_top = start; /* discard token from text buffer */
				}
			}
			// Now do extended tokens
			if (text_scan_data.extended == SCAN_P_IGNORE)
			{
				continue;
			}
			close = text_scan_data.id;
			nestlevel = 1;
			nestable = false;
			// find string after the ID in the prototype token
			while (text_bot[close++] != 0)
			{
			}
			switch (text_scan_data.extended)
			{
			case SCAN_P_CHAR:
				text_insert_char((char) text_char);
				text_insert_char('\0');
				text_get_char();
				text_scan_data.id = start;
				break;
			case SCAN_P_CHAR_ESC:
				if (text_char == text_bot[close]) // found escape character
				{
					// translate all C escapes. Anything else returns escaped
					// character
					text_get_char(); /* skip escape character */
					switch (text_char)
					{
					case 'n':
						text_insert_char('\n');
						text_get_char();
						break;
					case 't':
						text_insert_char('\t');
						text_get_char();
						break;
					case 'b':
						text_insert_char('\b');
						text_get_char();
						break;
					case 'r':
						text_insert_char('\r');
						text_get_char();
						break;
					case 'f':
						text_insert_char('\f');
						text_get_char();
						break;
					case 'x':
					case 'X': /* hexadecimal */
						start = text_top;
						do
						{
							text_get_char();
							text_insert_char((char) text_char);
						} while (isxdigit(text_char));
						text_top = 0;
						long temp = strtol(text_get_string(start), null, 16);
						text_top = start; /* scrub from buffer */
						if (temp > 255)
						{
							text_message(TEXT_WARNING_ECHO, "Hex escape sequence overflows eight bits: wrapping\n");
						}
						text_insert_char((char) (temp % 255));
						break;
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7': /* octal */
						start = text_top;
						do
						{
							text_insert_char((char) text_char);
							text_get_char();
						} while (text_char >= '0' && text_char <= '7');
						text_top = 0; /* change last character to a null */
						temp = strtol(text_get_string(start), null, 8);
						text_top = start; /* scrub from buffer */
						if (temp > 255)
						{
							text_message(TEXT_WARNING_ECHO, "Octal escape sequence overflows eight bits: wrapping\n");
						}
						text_insert_char((char) (temp % 255));
						break;
					default: /* any other quoted character returns itself */
						text_insert_char((char) text_char);
						text_get_char();
						break;
					}
				}
				else
				{
					text_insert_char((char) text_char);
					text_insert_char('\0');
					text_get_char();
				}
				text_insert_char('\0');
				text_scan_data.id = start;
				break;
			case SCAN_P_STRING:
				boolean loop;
				do
				{
					while (text_char != text_bot[text_scan_data.id])
					{
						if (text_char == '\n' || text_char == EOF)
						{
							text_message(TEXT_ERROR_ECHO, "Unterminated string\n");
							break;
						}
						text_insert_char((char) text_char);
						text_get_char();
					}
					text_get_char(); // get character after close
					loop = false;
					if (text_char == text_bot[text_scan_data.id])
					{
						text_insert_char((char) text_char);
						text_get_char();
						loop = true;
					}
				} while (loop);
				text_insert_char('\0');
				text_scan_data.id = start;
				break;
			case SCAN_P_STRING_ESC:
				while (text_char != text_bot[text_scan_data.id])
				{
					if (text_char == '\n' || text_char == EOF)
					{
						text_message(TEXT_ERROR_ECHO, "Unterminated string\n");
						break;
					}
					else if (text_char == text_bot[close]) // found escape
															// character
					{
						text_get_char(); /* skip escape character */
						switch (text_char)
						{
						case 'n':
							text_insert_char('\n');
							text_get_char();
							break;
						case 't':
							text_insert_char('\t');
							text_get_char();
							break;
						case 'b':
							text_insert_char('\b');
							text_get_char();
							break;
						case 'r':
							text_insert_char('\r');
							text_get_char();
							break;
						case 'f':
							text_insert_char('\f');
							text_get_char();
							break;
						case 'x':
						case 'X': /* hexadecimal */
							start = text_top;
							do
							{
								text_get_char();
								text_insert_char((char) text_char);
							} while (isxdigit(text_char));
							text_top = 0; // change last character to a null
							long temp = strtol(text_get_string(start), null, 16);
							text_top = start; /* scrub from buffer */
							if (temp > 255)
							{
								text_message(TEXT_WARNING_ECHO, "Hex escape sequence overflows eight bits: wrapping\n");
							}
							text_insert_char((char) (temp % 255));
							break;
						case '0':
						case '1':
						case '2':
						case '3':
						case '4':
						case '5':
						case '6':
						case '7': /* octal */
							start = text_top;
							do
							{
								text_insert_char((char) text_char);
								text_get_char();
							} while (text_char >= '0' && text_char <= '7');
							text_top = 0; // change last character to a null
							temp = strtol(text_get_string(start), null, 8);
							text_top = start; /* scrub from buffer */
							if (temp > 255)
							{
								text_message(TEXT_WARNING_ECHO,
										"Octal escape sequence overflows eight bits: wrapping\n");
							}
							text_insert_char((char) (temp % 255));
							break;
						default:
							text_insert_char((char) text_char);
							text_get_char();
							break;
						}
					}
					else
					{
						/* ordinary character */
						text_insert_char((char) text_char);
						text_get_char();
					}
				}
				text_get_char(); /* skip close character */
				text_insert_char('\0'); /* terminate string */
				text_scan_data.id = start; /* make current id string body */
				break;

			case SCAN_P_COMMENT_LINE:
			case SCAN_P_COMMENT_LINE_VISIBLE:
				while (text_char != '\n' && text_char != EOF)
				{
					text_insert_char((char) text_char);
					text_get_char();
				}
				text_insert_char('\0'); /* terminate with a null */
				text_scan_data.id = start; /* make current id comment body */
				if (text_scan_data.extended == SCAN_P_COMMENT_LINE)
				{
					text_scan_data.token = SCAN_P_IGNORE;
					if (retain_comments)
					{
						scan_insert_comment_block(text_get_string(start), last_column, scan_sequence_running_number);
					}
					else
					{
						text_top = start; // scrub the comment from text buffer
					}
				}
				break;
			case SCAN_P_COMMENT_NEST:
			case SCAN_P_COMMENT_NEST_VISIBLE:
				nestable = true;
			case SCAN_P_COMMENT_VISIBLE:
			case SCAN_P_COMMENT:
				// /* We have to be a bit careful here: remember that the
				// text_get_char() routine puts a space in at the start of each
				// line to
				// delay echoing of the line in the assembler */
				do
				{
					if (text_char == EOF)
					{
						text_message(TEXT_FATAL_ECHO, "Comment terminated by end of file\n");
					}
					if (last != '\n')
					{
						text_insert_char((char) text_char);
					}
					last = text_char;
					text_get_char();
					// single close or double close
					if (text_bot[close + 1] == 0 && text_bot[close] == text_bot[text_top - 1]
							|| text_bot[close + 1] == text_bot[text_top - 1]
							&& text_bot[close] == text_bot[text_top - 2])
					{
						nestlevel--;
					}
					else if (text_bot[text_scan_data.id + 1] == 0
							&& text_bot[text_scan_data.id] == text_bot[text_top - 1]
							|| text_bot[text_scan_data.id + 1] == text_bot[text_top - 1]
							&& text_bot[text_scan_data.id] == text_bot[text_top - 2])
					{
						nestlevel += nestable ? 1 : 0;
					}
				} while (nestlevel > 0);

				if (text_bot[close + 1] != 0)
				{
					text_top--; // backup one extra character
				}
				// backup over close and terminate with a null
				text_bot[text_top - 1] = 0;
				text_scan_data.id = start; // make current id comment body
				if (text_scan_data.extended == SCAN_P_COMMENT || text_scan_data.extended == SCAN_P_COMMENT_NEST)
				{
					text_scan_data.token = SCAN_P_IGNORE;
					if (retain_comments)
					{
						scan_insert_comment_block(text_get_string(start), last_column, scan_sequence_running_number);
					}
					else
					{
						text_top = start; // scrub the comment from text buffer
					}
				}
				break;
			default:
				break; /* do nothing */
			}
		} while (text_scan_data.token == SCAN_P_IGNORE);
		text_scan_data.comment_block = last_comment_block;
		if (scan_sequence_running_number != text_sequence_number())
		{
			scan_insert_comment_block(null, 0, text_sequence_number());
		}
		scan_sequence_running_number = text_sequence_number();
		text_scan_data.sourcefilename = rdp_sourcefilename;
		text_scan_data.line_number = text_line_number();
		if (scan_symbol_echo)
		{
			System.err.println("Scan symbol echo");
			text_message(TEXT_INFO, "Scanned ");
			// TODO set_print_element(text_scan_data.token, scan_token_names);
			// text_printf(" id \'%s\', sequence number %lu\n",
			// text_scan_data.id,
			// scan_sequence_running_number);
		}
		if (scan_lexicalise_flag)
		{
			System.err.println("Scan lexicalise flag");
			// TODO scan_token_count++;
			// if (strcmp(text_scan_data.id, "EOF") == 0)
			// text_printf("\n****** %u tokens\n", scan_token_count - 1);
			// else if (strcmp(text_scan_data.id, "EOLN") == 0)
			{
				text_printf("\n");
				// scan_token_count --;
			}
			// else if (text_scan_data.token == SCAN_P_ID)
			// text_printf("ID ");
			// else if (text_scan_data.token == SCAN_P_INTEGER)
			// text_printf("INTEGER ");
			// else if (text_scan_data.token == SCAN_P_REAL)
			// text_printf("REAL ");
			// else if (text_scan_data.extended == SCAN_P_STRING ||
			// text_scan_data.extended
			// == SCAN_P_STRING_ESC)
			// text_printf("STRING ");
			// else if (text_scan_data.extended == SCAN_P_CHAR ||
			// text_scan_data.extended
			// ==
			// SCAN_P_CHAR_ESC)
			// text_printf("CHAR ");
			// else if (text_scan_data.extended == SCAN_P_COMMENT_VISIBLE ||
			// text_scan_data.extended == SCAN_P_COMMENT_NEST_VISIBLE ||
			// text_scan_data.extended == SCAN_P_COMMENT_LINE_VISIBLE )
			// text_printf("COMMENT ");
			// else
			// text_printf("%s ", text_scan_data.id);
		}
	}

	static void scan_insert_comment_block(String pattern, int column, int sequence_number)
	{
		ScanCommentBlock temp = new ScanCommentBlock();
		scan_comment_list_end.comment = pattern;
		scan_comment_list_end.sequence_number = sequence_number;
		scan_comment_list_end.column = column;
		temp.previous = scan_comment_list_end;
		scan_comment_list_end.next = temp;
		scan_comment_list_end = temp;
		last_comment_block = temp;
	}
}
