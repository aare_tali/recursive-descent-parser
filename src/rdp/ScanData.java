package rdp;

class ScanData extends Symbol
{
	int token;
	int extended;
	ScanCommentBlock comment_block;
	String sourcefilename;
	int line_number;
	int u;
	int i;
	double r;
	Object p;

	@Override
	void memset()
	{
		super.memset();
		token = 0;
		extended = 0;
		comment_block = null;
		sourcefilename = null;
		line_number = 0;
		u = 0;
		i = 0;
		r = 0;
		p = null;
	}
}
