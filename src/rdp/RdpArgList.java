package rdp;

import rdp.Arg.ArgKind;

class RdpArgList
{
	ArgKind kind;
	String var;
	String key;
	String desc;
	RdpArgList next;
}
