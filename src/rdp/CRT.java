package rdp;

import static rdp.Text.*;

import java.io.*;

class CRT
{
	static void fclose(InputStream file)
	{
		try
		{
			file.close();
		}
		catch (IOException e)
		{
		}
	}

	static boolean feof(InputStream file)
	{
		try
		{
			return file.available() == 0;
		}
		catch (IOException e)
		{
		}
		return true;
	}

	static int getc(InputStream file)
	{
		try
		{
			return file.read();
		}
		catch (IOException e)
		{
		}
		return EOF;
	}

	static boolean isalnum(int ch)
	{
		return isalpha(ch) || isdigit(ch);
	}

	static boolean isalpha(int ch)
	{
		return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".indexOf(ch) >= 0;
	}

	static boolean isdigit(int ch)
	{
		return "0123456789".indexOf(ch) >= 0;
	}

	static boolean isgraph(char ch)
	{
		return ch > ' ';
	}

	static boolean isprint(int ch)
	{
		return ch >= ' ';
	}

	static boolean isspace(int ch)
	{
		return ch <= ' ';
	}

	static boolean isxdigit(int ch)
	{
		return "0123456789ABCDEFabcdef".indexOf(ch) >= 0;
	}

	static void memcpy(ScanData to, ScanData from)
	{
		to.id = from.id;
		to.token = from.token;
		to.extended = from.extended;
		to.comment_block = from.comment_block;
		to.sourcefilename = from.sourcefilename;
		to.line_number = from.line_number;
		to.u = from.u;
		to.i = from.i;
		to.r = from.r;
		to.p = from.p;
	}

	static int strcmp(String str1, String str2)
	{
		return str1.compareTo(str2);
	}

	static long strtol(String nptr, Pointer<String> endptr, int base)
	{
		if (endptr != null)
		{
			System.err.println("strtol: endptr is not implemented");
		}
		long result = Long.parseLong(nptr, base);
		return result;
	}
}
