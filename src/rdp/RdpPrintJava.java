package rdp;

import static rdp.RDP.*;
import static rdp.RdpAux.*;
import static rdp.Text.*;
import static rdp.Text.TextMessageType.*;

import java.io.*;

class RdpPrintJava extends RdpPrint
{
	void print(String path, String prefix, SymbolScopeData base, boolean compiler)
	{
		String baseName = path + "\\src\\" + prefix + "\\";
		// Main is always Main
		printMainJava(baseName + "Main.java", prefix);
		// The rest is language-specific
		baseName += prefix;
		rdp_indentation = 0;
		printPrefixKeywordJava(baseName + "Keyword.java", prefix);
		printPrefixScannerJava(baseName + "Scanner.java", prefix);
		printPrefixCompilerJava(baseName + "Compiler.java", prefix, base, compiler);
		printPrefixCompilerSetsJava(baseName + "CompilerSets.java", prefix, base);
	}

	private PrintStream createFile(String fileName)
	{
		PrintStream file = null;
		try
		{
			File f = new File(fileName.substring(0, fileName.lastIndexOf('\\')).toLowerCase());
			f.mkdir();
			file = new PrintStream(fileName);
			rdp_indentation = 0;
		}
		catch (FileNotFoundException e)
		{
		}
		return file;
	}

	private void printAstJava(String prefix, PrintStream file, String pkg, String astPath)
	{
		if (new File(astPath + "\\Ast.java").exists())
		{
			return;
		}
		int indent = rdp_indentation;
		PrintStream ast = createFile(astPath + "\\Ast.java");
		if (ast != null)
		{
			text_redirect(ast);
			println("package " + pkg + ".ast;");
			println();
			println("import java.io.*;");
			println("import java.util.*;");
			println();
			println("import " + prefix.toLowerCase() + ".*;");
			println();
			println("public class Ast");
			println("{");
			println("\tprotected ArrayList<Ast> astList = new ArrayList<>();");
			println("\tprotected " + prefix + "Compiler " + prefix.toLowerCase() + "Compiler;");
			println();
			println("\tpublic Ast(" + prefix + "Compiler " + prefix.toLowerCase() + "Compiler)");
			println("\t{");
			println("\t\tthis." + prefix.toLowerCase() + "Compiler = " + prefix.toLowerCase() + "Compiler;");
			println("\t}");
			println();
			println("\tpublic void add(Ast ast)");
			println("\t{");
			println("\t\tastList.add(ast);");
			println("\t}");
			println("");
			println("\tpublic void add(" + prefix + "Compiler " + prefix.toLowerCase() + "Compiler, " + prefix
					+ "Keyword lastsym)");
			println("\t{");
			println("\t\tastList.add(new AstSymbol(" + prefix.toLowerCase() + "Compiler, lastsym, "
					+ prefix.toLowerCase() + "Compiler.getLastId()));");
			println("\t}");
			println();
			println("\tprotected void print(PrintStream out)");
			println("\t{");
			println("\t\tout.println(getClass().getSimpleName());");
			println("\t}");
			println("}");
			text_redirect(file);
			ast.close();
		}
		else
		{
			System.err.println("Cannot create Ast.java");
		}
		rdp_indentation = indent;
	}

	private void printAstPrimary(String primary, PrintStream file, String astPath, String pkg, String prefix)
	{
		if (new File(astPath + "\\Ast" + primary + ".java").exists())
		{
			return;
		}
		int indent = rdp_indentation;
		PrintStream ast = createFile(astPath + "\\Ast" + primary + ".java");
		if (ast != null)
		{
			text_redirect(ast);
			println("package " + pkg + ".ast;");
			println();
			println("import " + pkg + ".*;");
			println();
			println("public class Ast" + primary + " extends Ast");
			println("{");
			println("\tpublic Ast" + primary + "(" + prefix + "Compiler " + prefix.toLowerCase() + "Compiler)");
			println("\t{");
			println("\t\tsuper(" + prefix.toLowerCase() + "Compiler);");
			println("\t}");
			println("}");
			text_redirect(file);
			ast.close();
		}
		else
		{
			System.err.println("Cannot create Ast.java");
		}
		rdp_indentation = indent;
	}

	private void printAstSymbolJava(String prefix, PrintStream file, String pkg, String astPath)
	{
		if (new File(astPath + "\\AstSymbol.java").exists())
		{
			return;
		}
		int indent = rdp_indentation;
		PrintStream ast = createFile(astPath + "\\AstSymbol.java");
		if (ast != null)
		{
			text_redirect(ast);
			println("package " + pkg + ".ast;");
			println();
			println("import java.io.*;");
			println();
			println("import " + pkg + ".*;");
			println();
			println("public class AstSymbol extends Ast");
			println("{");
			println("\tprivate " + prefix + "Keyword sym;");
			println("\tprivate String id;");
			println();
			println("\tpublic AstSymbol(" + prefix + "Compiler " + prefix.toLowerCase() + "Compiler, " + prefix
					+ "Keyword lastsym, String lastId)");
			println("\t{");
			println("\t\tsuper(" + prefix.toLowerCase() + "Compiler);");
			println("\t\tsym = lastsym;");
			println("\t\tid = lastId;");
			println("\t}");
			println();
			println("\t@Override");
			println("\tprotected void print(PrintStream out)");
			println("\t{");
			println("\t\tString s = sym.toString();");
			println("\t\tif (id.equals(s))");
			println("\t\t{");
			println("\t\t\tout.println(getClass().getSimpleName() + \" '\" + sym + \"'\");");
			println("\t\t}");
			println("\t\telse");
			println("\t\t{");
			println("\t\t\tout.println(getClass().getSimpleName() + \" '\" + sym + \"' '\" + id + \"'\");");
			println("\t\t}");
			println("\t}");
			println("}");
			text_redirect(file);
			ast.close();
		}
		else
		{
			System.err.println("Cannot create Ast.java");
		}
		rdp_indentation = indent;

	}

	private void printDefaultAction(RdpData prod, String default_action)
	{
		iprintln("else");
		iprintln("{");
		rdp_indentation++;
		iprintln("/* default action processing for " + text_get_string(prod.id) + " */");
		/* Now copy out default action */
		// disabled by -p option
		if (!rdp_parser_only.value() && default_action != null)
		{
			for (char ch : default_action.toCharArray())
			{
				if (ch == '\n')
				{
					println();
					indent();
				}
				else
				{
					print("" + ch);
				}
			}
			println(); /* terminate semantic actions tidily */
		}
		rdp_indentation--;
		iprintln("}");
	}

	private void printFunctionPrimary(RdpData production, boolean compiler)
	{
		if (production.kind != K_PRIMARY)
		{
			iprintln("// Non-primary?");
			return;
		}
		if (production.call_count == 0)
		{
			iprintln("// Unused?");
			return;
		}
		if (production.code_only != 0)
		{
			iprintln("// Code-only?");
			return;
		}

		if (production.ll1_violation != 0)
		{
			iprintln("// WARNING - an LL(1) violation was detected " + "at this point in the grammar");
		}

		/* In trace mode, add an entry message */
		if (rdp_trace.value())
		{
			iprintln("text_message(TEXT_INFO, \"Entered \'" + text_get_string(production.id) + "\'\\n\");");
			iprintln();
		}

		rdp_print_parser_alternate(production, production, compiler);

		// add error handling on exit
		iprintln("scanner.test(\"" + text_get_string(production.id) + "\", " + text_get_string(production.id)
				+ "_stop, " + text_get_string(production.id) + "_stop);");
	}

	private void printMainJava(String fileName, String prefix)
	{
		if (new File(fileName).exists())
		{
			return;
		}
		PrintStream file = createFile(fileName);
		if (file == null)
		{
			text_printf("Main.java creation error\n");
			return;
		}
		text_redirect(file);
		String pkg = prefix.toLowerCase();
		println("package " + pkg + ";");
		println();

		iprintln("public class Main");
		iprintln("{");
		rdp_indentation++;
		iprintln("public static void main(String[] args)");
		iprintln("{");
		rdp_indentation++;
		iprintln("System.out.println(\"Compiling\");");
		iprintln("" + prefix + "Compiler compiler = new " + prefix + "Compiler();");
		iprintln("compiler.Open(args.length == 0 ? \"test." + pkg + "\" : args[0]);");
		iprintln("compiler.Compile();");
		iprintln("System.out.println(\"Done\");");
		iprintln("System.exit(0);");
		rdp_indentation--;
		iprintln("}");
		rdp_indentation--;
		iprintln("}");
		text_redirect(System.out);
		file.close();
	}

	private void printParserTest(RdpData prod, RdpData primary)
	{
		if (!prod.contains_null)
		{
			indent();
			rdp_print_parser_test(prod.id, prod.first, text_get_string(primary.id));
			println(";");
		}
	}

	private void printPrefixCompilerJava(String fileName, String prefix, SymbolScopeData base, Boolean compiler)
	{
		PrintStream file = createFile(fileName);
		if (file == null)
		{
			text_printf("PrefixCompiler.Java creation error\n");
			return;
		}
		text_redirect(file);
		String pkg = prefix.toLowerCase();
		iprintln("package " + pkg + ";");
		iprintln();

		iprintln("import static " + pkg + "." + prefix + "Keyword.*;");
		if (compiler)
		{
			iprintln();
			iprintln("import java.util.*;");
			iprintln();
			iprintln("import base.*;");
			iprintln("import " + pkg + ".ast.*;");
			iprintln();
			String astPath = fileName.substring(0, fileName.indexOf("\\" + prefix)) + "\\" + prefix + "\\ast";
			File dir = new File(astPath);
			if (!dir.exists())
			{
				if (dir.mkdir())
				{
					printAstJava(prefix, file, pkg, astPath);
				}
				else
				{
					System.err.println("Cannot create the AST directory");
				}
			}
			else if (!dir.isDirectory())
			{
				System.err.println("AST directory is not a directory");
			}
			else
			{
				File ast = new File(astPath + "\\Ast.java");
				if (!ast.exists())
				{
					printAstJava(prefix, file, pkg, astPath);
				}
			}
			File ast = new File(astPath + "\\AstSymbol.java");
			if (!ast.exists())
			{
				printAstSymbolJava(prefix, file, pkg, astPath);
			}
		}
		else
		{
			iprintln();
		}

		iprintln("public class " + prefix + "Compiler extends " + prefix + "CompilerSets");
		iprintln("{");
		rdp_indentation++;
		iprintln("public Ast" + text_get_string(rdp_start_prod.id) + " mainAst;");
		iprintln();
		iprintln("public " + prefix + "Compiler()");
		iprintln("{");
		iprintln("\tsuper(new " + prefix + "Scanner());");
		iprintln("}");
		iprintln();
		iprintln("public List<CompileError> getErrors()");
		iprintln("{");
		iprintln("\treturn scanner.getErrors();");
		iprintln("}");
		iprintln();
		iprintln("public String getLastId()");
		iprintln("{");
		iprintln("\treturn scanner.getLastId();");
		iprintln("}");
		iprintln();
		iprintln("@Override");
		iprintln("public Ast" + text_get_string(rdp_start_prod.id) + " Compile()");
		iprintln("{");
		if (compiler)
		{
			iprintln("\tAst" + text_get_string(rdp_start_prod.id) + " ast = " + text_get_string(rdp_start_prod.id)
					+ "();");
			iprintln("\treturn ast;");
		}
		else
		{
			iprintln("\t" + text_get_string(rdp_start_prod.id) + "();");
			iprintln("\treturn null;");
		}
		iprintln("}");

		RdpData temp = (RdpData) base.nextSymbolInScope();
		while (temp != null)
		{
			if (temp.kind == K_PRIMARY && temp.call_count > 0)
			{
				if (temp.code_only != 0)
				{
					text_printf("Text code-only " + text_get_string(temp.id) + "");
				}
				else
				{
					iprintln();
					if (compiler)
					{
						iprintln("protected Ast" + text_get_string(temp.id) + " " + text_get_string(temp.id) + "()");
						String astPath = fileName.substring(0, fileName.indexOf("\\" + prefix)) + "\\" + prefix
								+ "\\ast";
						printAstPrimary(text_get_string(temp.id), file, astPath, pkg, prefix);
					}
					else
					{
						iprintln("protected void " + text_get_string(temp.id) + "()");
					}
					iprintln("{");
					rdp_indentation++;
					if (compiler)
					{
						iprintln("Ast" + text_get_string(temp.id) + " ast = new Ast" + text_get_string(temp.id)
								+ "(this);");
					}
					printFunctionPrimary(temp, compiler);
					if (compiler)
					{
						iprintln("return ast;");
					}
					rdp_indentation--;
					iprintln("}");
				}
			}
			temp = (RdpData) temp.nextSymbolInScope();
		}
		rdp_indentation--;
		iprintln("}");
		text_redirect(System.out);
		file.close();
	}

	private void printPrefixCompilerSetsJava(String fileName, String prefix, SymbolScopeData base)
	{
		PrintStream file = createFile(fileName);
		if (file == null)
		{
			text_printf("PrefixCompilerSets.Java creation error\n");
			return;
		}
		text_redirect(file);
		String pkg = prefix.toLowerCase();
		iprintln("package " + pkg + ";");
		iprintln();
		iprintln("import static " + pkg + "." + prefix + "Keyword.*;");
		iprintln();
		iprintln("import base.*;");
		iprintln("import " + pkg + ".ast.*;");
		iprintln();

		iprintln("abstract class " + prefix + "CompilerSets extends " + "BaseCompiler<" + prefix + "Keyword, Ast>");
		iprintln("{");
		rdp_indentation++;

		// print set declaration
		RdpData temp = (RdpData) base.nextSymbolInScope();
		while (temp != null)
		{
			if (rdp_production_set.includes(temp.kind) && temp.code_only == 0)
			{
				if (temp.first_cardinality > 1)
				{
					iprintln("protected final Set<" + prefix + "Keyword> " + text_get_string(temp.id)
							+ "_first = new Set<" + prefix + "Keyword>();");
				}
				if (temp.kind == K_PRIMARY)
				{
					iprintln("protected final Set<" + prefix + "Keyword> " + text_get_string(temp.id)
							+ "_stop = new Set<" + prefix + "Keyword>();");
				}
			}
			temp = (RdpData) temp.nextSymbolInScope();
		}
		iprintln();

		iprintln("protected " + prefix + "CompilerSets(BaseScanner<" + prefix + "Keyword> scanner)");
		iprintln("{");
		rdp_indentation++;
		iprintln("super(scanner);");
		temp = (RdpData) base.nextSymbolInScope();
		while (temp != null)
		{
			if (rdp_production_set.includes(temp.kind) && temp.code_only == 0)
			{
				if (temp.first_cardinality > 1)
				{
					int indent = iprint(text_get_string(temp.id) + "_first.init(new " + prefix + "Keyword[] {");
					temp.first.printIndented(rdp_enum_string, indent, 80);
					println(" });");
				}
				if (temp.kind == K_PRIMARY)
				{
					int indent = iprint(text_get_string(temp.id) + "_stop.init(new " + prefix + "Keyword[] {");
					temp.follow.printIndented(rdp_enum_string, indent, 80);
					println(" });");
				}
			}
			temp = (RdpData) temp.nextSymbolInScope();
		}
		rdp_indentation--;
		iprintln("}");
		rdp_indentation--;
		iprintln("}");
		text_redirect(System.out);
		file.close();
	}

	private void printPrefixKeywordJava(String fileName, String prefix)
	{
		PrintStream file = createFile(fileName);
		if (file == null)
		{
			text_printf("PrefixKeyword.java creation error\n");
			return;
		}
		text_redirect(file);
		String pkg = prefix.toLowerCase();
		iprintln("package " + pkg + ";");
		iprintln();

		iprintln("public enum " + prefix + "Keyword");
		iprintln("{");
		rdp_indentation++;
		iprintln("SCAN_P_EOF(\"<EOF>\"),");
		iprintln("SCAN_P_ID(\"<Ident>\"),");
		iprintln("SCAN_P_ERROR(\"<Error>\"),");
		iprintln("SCAN_P_INTEGER(\"<Integer>\"),");
		iprintln("SCAN_P_REAL(\"<Float>\"),");
		RdpData temp = (RdpData) tokens.getScope().nextSymbolInScope();
		while (temp != null)
		{
			if (temp.kind == K_TOKEN || temp.kind == K_EXTENDED)
			{
				iprint("");
				rdp_print_parser_production_name(temp);
				String id = "";
				String tempId = text_get_string(temp.id);
				for (char ch : tempId.toCharArray())
				{
					if (ch == '"')
					{
						id += "\\\"";
					}
					else
					{
						id += ch;
					}
				}
				if (id.equals("\\\""))
				{
					id = "<String>";
				}
				if (id.equals("\'"))
				{
					id = "<Char>";
				}
				text_printf("(\"" + id + "\"),");
				iprintln();
			}
			temp = (RdpData) temp.nextSymbolInScope();
		}
		iprintln("kw__last(null);");
		iprintln();
		iprintln("private String text;");
		iprintln();
		iprintln("private " + prefix + "Keyword(String string)");
		iprintln("{");
		iprintln("\ttext = string;");
		iprintln("}");
		iprintln();
		iprintln("@Override");
		iprintln("public String toString()");
		iprintln("{");
		iprintln("\treturn text;");
		iprintln("}");
		rdp_indentation--;
		iprintln("}");
		text_redirect(System.out);
		file.close();
	}

	private void printPrefixScannerJava(String fileName, String prefix)
	{
		PrintStream file = createFile(fileName);
		if (file == null)
		{
			text_printf("PrefixScanner.java creation error\n");
			return;
		}
		text_redirect(file);
		String pkg = prefix.toLowerCase();
		iprintln("package " + pkg + ";");
		iprintln();

		iprintln("import static " + pkg + "." + prefix + "Keyword.*;");
		iprintln();
		iprintln("import base.*;");
		iprintln();

		iprintln("class " + prefix + "Scanner extends BaseScanner<" + prefix + "Keyword>");
		iprintln("{");
		rdp_indentation++;
		iprintln("public " + prefix + "Scanner()");
		iprintln("{");
		rdp_indentation++;
		iprintln("super(new " + prefix + "Preprocessor());");
		rdp_indentation--;
		iprintln("}");
		iprintln();
		iprintln("@Override");
		iprintln("public " + prefix + "Keyword TK_EOF()");
		iprintln("{");
		iprintln("\treturn SCAN_P_EOF;");
		iprintln("}");
		iprintln();
		iprintln("@Override");
		iprintln("public " + prefix + "Keyword TK_ERROR()");
		iprintln("{");
		iprintln("\treturn SCAN_P_ERROR;");
		iprintln("}");
		iprintln();
		iprintln("@Override");
		iprintln("public " + prefix + "Keyword TK_FCONST()");
		iprintln("{");
		iprintln("\treturn SCAN_P_REAL;");
		iprintln("}");
		iprintln();
		iprintln("@Override");
		iprintln("public " + prefix + "Keyword TK_ICONST()");
		iprintln("{");
		iprintln("\treturn SCAN_P_INTEGER;");
		iprintln("}");
		iprintln();
		iprintln("@Override");
		iprintln("public " + prefix + "Keyword TK_ID()");
		iprintln("{");
		iprintln("\treturn SCAN_P_ID;");
		iprintln("}");
		iprintln();
		iprintln("@Override");
		iprintln("public " + prefix + "Keyword TK_CCONST()");
		iprintln("{");
		iprintln("\treturn RDP_T_39;");
		iprintln("}");
		iprintln();
		iprintln("@Override");
		iprintln("public " + prefix + "Keyword TK_SCONST()");
		iprintln("{");
		iprintln("\treturn RDP_T_34;");
		iprintln("}");
		// iprintln();
		// iprintln("/**");
		// iprintln(" * Get a decimal integer constant. Basic scanner does "
		// + "not support floating");
		// iprintln(" * point values. lastsym is set to tk_iconst. lastid is
		// " + "set to the sequence");
		// iprintln(" * of decimal digit characters in the integer.");
		// iprintln(" */");
		// iprintln("protected void GetNumber()");
		// iprintln("{");
		// javaIndent++;
		// iprintln("lastid = \"\";");
		// iprintln("lastid += (char) lastch;");
		// iprintln("getchar();");
		// iprintln("if ((lastch == 'x') || (lastch == 'X'))");
		// iprintln("{");
		// iprintln("\tgetchar();");
		// iprintln("\twhile (IsXDigit())");
		// iprintln("\t{");
		// iprintln("\t\tlastid += lastch;");
		// iprintln("\t\tgetchar();");
		// iprintln("\t}");
		// iprintln("}");
		// iprintln("else");
		// iprintln("{");
		// iprintln("\twhile (IsDigit())");
		// iprintln("\t{");
		// iprintln("\t\tlastid += lastch;");
		// iprintln("\t\tgetchar();");
		// iprintln("\t}");
		// iprintln("}");
		// iprintln("lastsym = TK_ICONST();");
		// javaIndent--;
		// iprintln("}");
		iprintln();

		iprintln("@Override");
		iprintln("protected void InitAttributes()");
		iprintln("{");
		rdp_indentation++;
		iprintln("SetAttributes(\"01234567\", A_PRINT | A_IDCHAR2 | " + "A_HEX | A_DIGIT | A_OCTAL);");
		iprintln("SetAttributes(\"89\", A_PRINT | A_IDCHAR2 | " + "A_HEX | A_DIGIT);");
		iprintln("SetAttributes(\"ABCDEFabcdef\", A_PRINT | A_IDCHAR | " + "A_IDCHAR2 | A_HEX);");
		iprintln("SetAttributes(\"GHIJKLMNOPQRSTUVWXYZghijklmnopqrstu" + "vwxyz_\", A_PRINT");
		iprintln("\t\t| A_IDCHAR | A_IDCHAR2);");
		iprintln("SetAttributes(\" \\f\\r\\t\", A_SPACE);");
		iprintln("SetAttributes(\"+*-/%?:;=<>|&~!^()[]{}.,$\", A_PRINT | " + "A_OPERATOR);");
		rdp_indentation--;
		iprintln("}");
		iprintln();

		// iprintln("/**");
		// iprintln(" * Initialize the keywords from XplKeyword enum. All "
		// + "keywords are added if");
		// iprintln(" * they are shorter than 3 letters, or they are longer
		// " + "than 3 letters but");
		// iprintln(" * don't have < in the beginning and > in the end,
		// which " + "excludes");
		// iprintln(" * pseudo-tokens for EOF, identifier, constants
		// etc.");
		// iprintln(" */");

		iprintln("@Override");
		iprintln("protected void InitKeywords()");
		iprintln("{");
		rdp_indentation++;
		RdpData temp = (RdpData) tokens.getScope().nextSymbolInScope();
		while (temp != null)
		{
			if (temp.kind == K_TOKEN || temp.kind == K_EXTENDED)
			{
				if (!text_get_string(temp.id).equals("\"") && !text_get_string(temp.id).equals("\'"))
				{
					iprint("AddKeyword(");
					rdp_print_parser_production_name(temp);
					println(");");
				}
				else if (temp.extended_enum.equals("SCAN_P_STRING_ESC"))
				{
					iprintln("AddKeyword(RDP_T_34);");
				}
				else if (temp.extended_enum.equals("SCAN_P_CHAR_ESC"))
				{
					iprintln("AddKeyword(RDP_T_39);");
				}
				else
				{
					System.out.println("something");
				}
			}
			temp = (RdpData) temp.nextSymbolInScope();
		}
		rdp_indentation--;
		iprintln("}");
		rdp_indentation--;
		iprintln("}");
		text_redirect(System.out);
		file.close();
	}

	private void rdp_print_parser_alternate(RdpData production, RdpData primary, boolean compiler)
	{
		RdpList list = production.list;
		if (list.next == null)
		{
			rdp_print_parser_sequence(list.production, primary, compiler);
		}
		else
		{
			boolean head = true;
			while (list != null)
			{
				if (list.production.kind != K_SEQUENCE)
				{
					text_message(TEXT_FATAL, "internal error - expecting alternate\n");
				}
				if (head)
				{
					indent();
					head = false;
				}
				else
				{
					print(" ");
				}
				print("if (");
				rdp_print_parser_test(list.production.id, list.production.first, null);
				println(")");
				iprintln("{");
				rdp_indentation++;
				rdp_print_parser_sequence(list.production, primary, compiler);
				rdp_indentation--;
				iprintln("}");
				list = list.next;
				if (list != null)
				{
					iprint("else");
				}
				else if (!(production.contains_null && production.lo != 0))
				{
					/* tail test at end of alternates */
					iprintln("else");
					iprintln("{");
					rdp_indentation++;
					indent();
					rdp_print_parser_test(production.id, production.first, text_get_string(primary.id));
					println(";");
					rdp_indentation--;
					iprintln("}");
				}
			}
		}
	}

	private void rdp_print_parser_item(RdpData prod, RdpData primary, String return_name, RdpParamList actuals,
			int promote_epsilon, int promote, String default_action, boolean compiler)
	{
		if (promote == PROMOTE_DEFAULT)
		{
			promote = prod.promote_default;
		}

		// if (!(prod.kind == K_CODE && prod.code_successor != 0))
		// {
		// /* Don't indent code sequence-internal or inline items */
		// indent();
		// }

		switch (prod.kind)
		{
		case K_INTEGER:
		case K_REAL:
		case K_STRING:
		case K_EXTENDED:
		case K_TOKEN:
			iprint("scanner.test(\"" + text_get_string(primary.id) + "\", ");
			rdp_print_parser_production_name(prod);
			println(", " + text_get_string(primary.id) + "_stop);");
			/* disable if -p option used */
			if (return_name != null && !rdp_parser_only.value())
			{
				iprintln(return_name + " = SCAN_CAST."
						+ (prod.kind == K_REAL ? "data.r" : prod.kind == K_INTEGER ? "data.i" : "id") + ";");
			}
			if (compiler)
			{
				iprintln("ast.add(this, lastsym);");
			}
			iprintln("getsym();");
			break;
		case K_CODE:
			if (!rdp_parser_only.value()) /* disabled by -p option */
			{
				String temp = text_get_string(prod.id);

				if (prod.code_pass != 0)
				{
					iprintln("if (rdp_pass == " + prod.code_pass + ")");
					iprintln("{");
				}

				for (char ch : temp.toCharArray())
				{
					if (ch == '\n')
					{
						text_printf(" \\\n");
					}
					else
					{
						text_printf("" + ch);
					}
				}

				if (prod.code_pass != 0)
				{
					iprintln("}");
				}

				if (prod.kind == K_CODE && prod.code_terminator != 0)
				{
					iprintln(); /* terminate semantic actions tidily */
				}
			}
			break;
		case K_PRIMARY:
			if (return_name != null && !rdp_parser_only.value())
			{
				iprint(return_name + " = ");
			}
			else
			{
				indent();
			}
			if (compiler)
			{
				print("ast.add(");
			}
			print(text_get_string(prod.id));
			if (!(prod.code_only != 0 && actuals == null))
			{
				rdp_print_parser_param_list(promote == PROMOTE_DONT ? text_get_string(prod.id) : null, actuals, 0, 0);
			}
			if (compiler)
			{
				print(")");
			}
			println(";");
			break;
		case K_SEQUENCE:
			text_message(TEXT_FATAL, "internal error - unexpected alternate in sequence\n");
			break;
		case K_LIST:
			rdp_print_parser_subproduction(prod, primary, promote_epsilon, default_action, compiler);
			break;
		default:
			text_message(TEXT_FATAL, "internal error - unexpected kind found\n");
		}
	}

	private void rdp_print_parser_param_list(String first, RdpParamList params, int definition, int start_rule)
	{
		text_printf("(");

		if (params == null && definition != 0 && rdp_dir_tree == 0)
		{
			text_printf("void");
		}
		else
		{
			rdp_print_parser_param_list_sub(params, 1, definition);
		}

		text_printf(")");
	}

	private void rdp_print_parser_param_list_sub(RdpParamList param, int last, int definition)
	{
		if (param != null)
		{
			rdp_print_parser_param_list_sub(param.next, 0, definition);
			text_printf(definition != 0 ? param.type : "");

			if (definition != 0)
			{
				for (int count = 0; count < param.stars; count++)
				{
					text_printf("*");
				}
			}

			text_printf(definition != 0 ? " " : "");
			switch (param.flavour)
			{
			case PARAM_INTEGER:
				text_printf(Integer.toString(param.n));
				break;
			case PARAM_REAL:
				text_printf(Double.toString(param.r));
				break;
			case PARAM_STRING:
				text_printf("\"" + param.id + "\"");
				break;
			case PARAM_ID:
				text_printf(param.id);
				break;
			}
			text_printf(last != 0 ? "" : ", ");
		}
	}

	private void rdp_print_parser_sequence(RdpData production, RdpData primary, boolean compiler)
	{
		for (RdpList list = production.list; list != null; list = list.next)
		{
			rdp_print_parser_item(list.production, primary, list.return_name, list.actuals, list.promote_epsilon,
					list.promote, list.default_action, compiler);
		}
	}

	private void rdp_print_parser_subproduction(RdpData prod, RdpData primary, int promote_epsilon,
			String default_action, boolean compiler)
	{
		// iprintln("// sub: lo " + prod.lo + ", hi " + prod.hi);
		if (prod.ll1_violation != 0)
		{
			iprintln("// WARNING - an LL(1) violation was detected at this" + " point in the grammar");
		}
		if (prod.lo == 1 && prod.hi == 1)
		{
			printParserTest(prod, primary);
			rdp_print_parser_alternate(prod, primary, compiler);
		}
		if (prod.lo == 0 && prod.hi == 1)
		{
			iprint("if (");
			rdp_print_parser_test(prod.id, prod.first, null);
			println(")");
			iprintln("{");
			rdp_indentation++;
			rdp_print_parser_alternate(prod, primary, compiler);
			rdp_indentation--;
			iprintln("}");
			if (rdp_dir_tree != 0 || default_action != null)
			{
				printDefaultAction(prod, default_action);
			}
		}
		if (prod.lo == 0 && prod.hi == 0)
		{
			iprint("if (");
			rdp_print_parser_test(prod.id, prod.first, null);
			println(")");
			iprintln("{");
			rdp_indentation++;
			iprintln("while (true)");
			iprintln("{");
			rdp_indentation++;
			rdp_print_parser_alternate(prod, primary, compiler);
			if (prod.supplementary_token != null)
			{
				String tokenName = text_get_string(prod.supplementary_token.token_enum);
				// TODO tokenName = FixUpTokenName(tokenName);
				iprintln("if (lastsym != " + tokenName + ")");
				iprintln("{");
				iprintln("\tbreak;");
				iprintln("}");
				if (compiler)
				{
					iprintln("ast.add(this, lastsym);");
				}
				iprintln("getsym();"); /* skip list token */
			}
			else
			{
				iprint("if (!");
				rdp_print_parser_test(prod.id, prod.first, null);
				println(")");
				iprintln("{");
				iprintln("\tbreak;");
				iprintln("}");
			}
			rdp_indentation--;
			iprintln("}");
			rdp_indentation--;
			iprintln("}");
			if (rdp_dir_tree != 0 || default_action != null)
			{
				printDefaultAction(prod, default_action);
			}
		}
		if (prod.lo == 1 && prod.hi == 0)
		{
			iprintln("while (true)");
			iprintln("{");
			rdp_indentation++;
			indent();
			rdp_print_parser_test(prod.id, prod.first, text_get_string(primary.id));
			println(";");
			rdp_print_parser_alternate(prod, primary, compiler);
			if (prod.supplementary_token != null)
			{
				String tokenName = text_get_string(prod.supplementary_token.token_enum);
				// TODO tokenName = FixUpTokenName(tokenName);
				iprintln("if (lastsym != " + tokenName + ")");
				iprintln("{");
				iprintln("\tbreak;");
				iprintln("}");
				if (compiler)
				{
					iprintln("ast.add(this, lastsym);");
				}
				iprintln("getsym();"); /* skip list token */
			}
			else
			{
				iprint("if (!");
				rdp_print_parser_test(prod.id, prod.first, null);
				println(")");
				iprintln("{");
				iprintln("\tbreak;");
				iprintln("}");
			}
			rdp_indentation--;
			iprintln("}");
		}
	}

	private void rdp_print_parser_test(int first_name, Set first, String follow_name)
	{
		text_printf("scanner.test(\"" + text_get_string(first_name) + "\", ");

		if (first.cardinality() > 1)
		{
			text_printf(text_get_string(first_name) + "_first");
		}
		else
		{
			first.print(rdp_enum_string, 78);
		}

		if (follow_name == null)
		{
			text_printf(", null)");
		}
		else
		{
			text_printf(", " + follow_name + "_stop)");
		}
	}
}
