package rdp;

import static rdp.Text.*;

import java.util.*;

class Set
{
	static final int SET_END = -1;

	static int set_cardinality(Set src)
	{
		return src == null ? 0 : src.cardinality();
	}

	static int set_print_element(int element, String[] element_names)
	{
		if (element_names == null)
		{
			return text_printf(Integer.toString(element));
		}
		else
		{
			return text_printf(element_names[element]);
		}
	}

	private long[] data = new long[10];

	Integer[] array()
	{
		ArrayList<Integer> elements = new ArrayList<Integer>();
		for (int word = 0; word < data.length; word++)
		{
			for (int bit = 0; bit < 32; bit++)
			{
				if ((data[word] & 1 << bit) != 0)
				{
					elements.add(word * 32 + bit);
				}
			}
		}
		return elements.toArray(new Integer[0]);
	}

	/** clear a dst and then set only those bits specified by src */
	void assign(int element)
	{
		clear();
		set(element);
	}

	void assignList(int... bits)
	{
		clear();
		for (int bit : bits)
		{
			set(bit);
		}
	}

	/** assign one set to another */
	void assignSet(Set src)
	{
		clear();
		unite(src);
	}

	int cardinality()
	{
		int[] bitCounts = new int[] { 0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4 };

		int cardinality = 0;
		for (long bits : data)
		{
			for (int i = 0; i < 8; i++)
			{
				cardinality += bitCounts[(int) (bits & 0xF)];
				bits >>= 4;
			}
		}
		return cardinality;
	}

	void clear()
	{
		for (int i = 0; i < data.length; i++)
		{
			data[i] = 0;
		}
	}

	boolean includes(int element)
	{
		grow(element);
		int index = element / 32;
		element &= 0x1F;
		return (data[index] & 1 << element) != 0;
	}

	void intersect(Set src)
	{
		/* only iterate over shortest set */
		int length = length() < src.length() ? length() : src.length();
		for (int i = 0; i < length; i++)
		{
			data[i] &= src.data[i];
		}
		/* Now clear rest of dst */
		while (length < length())
		{
			data[length++] = 0;
		}
	}

	int length()
	{
		return data.length;
	}

	void print(String[] element_names, int line_length)
	{
		int column = 0;
		boolean not_first = false;
		Integer[] elements = array();
		for (int element : elements)
		{
			if (not_first)
			{
				column += text_printf(", ");
			}
			else
			{
				not_first = true;
			}

			if (line_length != 0 && column >= line_length)
			{
				text_printf("\n");
				column = 0;
			}
			column += set_print_element(element, element_names);
		}
	}

	void printIndented(String[] element_names, int indent, int line_length)
	{
		int column = 0;
		boolean not_first = false;
		Integer[] elements = array();
		for (int element : elements)
		{
			if (not_first)
			{
				column += text_printf(", ");
			}
			else
			{
				not_first = true;
			}

			if (line_length != 0 && column >= line_length)
			{
				text_printf("\n");
				// TODO Java tab width
				column = text_indent(indent + 2) * 4;
			}
			column += set_print_element(element, element_names);
		}
	}

	void printIndented(String[] element_names, int indent, int column, int length)
	{
		column += indent * 4;
		boolean not_first = false;
		Integer[] elements = array();
		for (int element : elements)
		{
			if (not_first)
			{
				column += text_printf(",");
			}
			else
			{
				not_first = true;
			}
			int textLength = element_names[element].length();
			if (column + textLength + 2 > length)
			{
				text_printf("\n");
				// TODO Java tab width
				column = text_indent(indent + 2);
			}
			else
			{
				column += text_printf(" ");
			}
			column += set_print_element(element, element_names);
		}
	}

	void set(int element)
	{
		grow(element);
		int index = element / 32;
		element &= 0x1F;
		data[index] |= 1 << element;
	}

	void unite(Set src)
	{
		grow(src.length());
		for (int i = 0; i < data.length; i++)
		{
			data[i] |= src.data[i];
		}
	}

	private void grow(int bits)
	{
		int index = (bits + 31) / 32;
		if (index >= data.length)
		{
			long[] newData = new long[index + 5];
			for (int i = 0; i < newData.length; i++)
			{
				newData[i] = 0;
			}
			for (int i = 0; i < data.length; i++)
			{
				newData[i] = data[i];
			}
			data = newData;
		}
	}
}
