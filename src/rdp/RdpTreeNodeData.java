package rdp;

class RdpTreeNodeData extends GraphNode
{
	final ScanData data = new ScanData();

	@Override
	int getId()
	{
		return data.id;
	}
}
