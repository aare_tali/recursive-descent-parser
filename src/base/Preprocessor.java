package base;

import java.io.*;

public class Preprocessor
{
	protected String fileName;
	protected BufferedReader inputFile;
	private BaseScanner<?> scanner;

	public Preprocessor()
	{
		inputFile = null;
	}

	public void close()
	{
		try
		{
			if (inputFile != null)
			{
				inputFile.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		inputFile = null;
	}

	/**
	 * Open a preprocessor input file. Function creates new input file entry. If
	 * input file entry existed when the function is called, existing entry is
	 * deleted first. Preprocessor can only have one input file entry, in case
	 * of nested input files derived preprocessor may have to handle file
	 * nesting.
	 *
	 * @param fn
	 *            File name.
	 * @return true if no errors.
	 */
	public boolean Open(String fn)
	{
		fileName = fn;
		try
		{
			inputFile = new BufferedReader(new FileReader(fn));
		}
		catch (FileNotFoundException e)
		{
			return false;
		}
		return true;
	}

	/**
	 * Read a line from the input file.
	 *
	 * @return String from the file, or null on error or eof.
	 */
	public String readLine()
	{
		if (inputFile == null)
		{
			return null;
		}
		try
		{
			scanner.readPos = 0;
			scanner.readNum++;
			return inputFile.readLine();
		}
		catch (IOException e)
		{
			return null;
		}
	}

	public void setScanner(BaseScanner<?> scanner)
	{
		this.scanner = scanner;
	}
}
