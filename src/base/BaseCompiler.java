package base;

public abstract class BaseCompiler<Keyword, Ast>
{
	protected BaseScanner<Keyword> scanner;
	protected Keyword lastsym;
	protected String lastid;

	public BaseCompiler(BaseScanner<Keyword> scanner)
	{
		this.scanner = scanner;
	}

	/**
	 * Compile the program. BaseCompiler does not handle compilation, derived classes are expected to figure out which
	 * one is the top level compilation rule.
	 *
	 * @return true if no errors.
	 */
	public abstract Ast Compile();

	public Location createLocation()
	{
		return scanner.createLocation();
	}

	/**
	 * Open: Open an input file. Parameters are passed to Scanner.
	 *
	 * @param fileName
	 *            File name.
	 * @return true if no errors.
	 */
	public boolean Open(String fileName)
	{
		boolean result = scanner.Open(fileName);
		if (result)
		{
			getsym();
		}
		return result;
	}

	protected void getsym()
	{
		scanner.getsym();
		lastsym = scanner.lastsym;
		lastid = scanner.lastid;
	}
}
