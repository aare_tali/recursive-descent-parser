package base;

public class CompileError
{
	public final String message;
	public final Location location;

	public CompileError(String message, Location location)
	{
		this.message = message;
		this.location = location;
	}

	public boolean isAt(String fileName, int lineNum, int linePos)
	{
		return location.fileName.equals(fileName) && location.lineNum == lineNum && location.linePos == linePos;
	}
}
