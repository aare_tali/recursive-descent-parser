package base;

public class Location
{

	public final String fileName;
	public final int lineNum;
	public final int linePos;

	public Location(String fileName, int lineNum, int linePos)
	{
		this.fileName = fileName;
		this.lineNum = lineNum;
		this.linePos = linePos;
	}
}
