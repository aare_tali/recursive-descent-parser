package base;

import java.util.*;

public abstract class BaseScanner<Keyword>
{
	/**
	 * Character is printable.
	 */
	protected final static int A_PRINT = 0x0001;

	/**
	 * Character is the first letter of a valid identifier.
	 */
	protected final static int A_IDCHAR = 0x0002;

	/**
	 * Character is the next letter of a valid identifier.
	 */
	protected final static int A_IDCHAR2 = 0x0004;

	/**
	 * Character is a decimal digit.
	 */
	protected final static int A_DIGIT = 0x0008;
	/**
	 * Character is a whitespace.
	 */
	protected final static int A_SPACE = 0x0010;
	/**
	 * Character is part of some operator. Operator may contain any character from this set, next characters are defined
	 * by a language-specific scanner.
	 */
	protected final static int A_OPERATOR = 0x0020;
	/**
	 * Character is a hex digit.
	 */
	protected final static int A_HEX = 0x0040;
	/**
	 * Character is an octal digit.
	 */
	protected final static int A_OCTAL = 0x0080;
	/**
	 * Placeholder for next available bit.
	 */
	protected final static int A_NEXT = 0x0100;

	// TODO static scanner
	// protected static BaseScanner<?> scanner;
	//
	// public static String getFileName()
	// {
	// return scanner.fileName;
	// }
	//
	// public static int getLineNum()
	// {
	// return scanner.lineNum;
	// }
	//
	// public static int getLinePos()
	// {
	// return scanner.linePos;
	// }

	protected String lastid;
	protected String lastLine;
	protected String lastFullLine;
	protected char lastch;
	protected Keyword lastsym;
	protected Map<String, Keyword> keywords;
	protected boolean initialized;
	protected Preprocessor preprocessor;
	protected Map<String, Integer> chtable;
	/** The file name being read and parsed. */
	protected String fileName;
	/** The line number in the input file. */
	protected int lineNum;
	/** The line number being read. */
	protected int readNum;
	/**
	 * The position in the line where first character for the symbol is being read.
	 */
	protected int linePos;

	/** Position of the character being read. */
	protected int readPos;

	private ArrayList<CompileError> errors;

	public BaseScanner(Preprocessor preprocessor)
	{
		// TODO static scanner
		// if (scanner != null)
		// {
		// System.err.println("Multiple scanners?");
		// }
		// scanner = this;
		keywords = new HashMap<>();
		lastLine = "";
		initialized = false;
		this.preprocessor = preprocessor;
		preprocessor.setScanner(this);
		chtable = new HashMap<>();
		errors = new ArrayList<>();
	}

	public List<CompileError> getErrors()
	{
		return errors;
	}

	public String getLastId()
	{
		return lastid;
	}

	/**
	 * Get a symbol from input stream. lastsym is set to token code of the token read. lastid is set to character
	 * representation of it.
	 */
	public void getsym()
	{
		for (;;)
		{
			if (lastch == (char) -1)
			{
				lastsym = TK_EOF();
				return;
			}
			if (IsSpace())
			{
				getchar();
				continue;
			}
			break;
		}
		lineNum = readNum;
		linePos = readPos;
		if (IsDelimiter())
		{
			GetString();
		}
		else if (IsIdent())
		{
			GetIdentifier();
		}
		else if (IsDigit())
		{
			GetNumber();
		}
		else if (IsOperator())
		{
			GetOperator();
		}
		else
		{
			System.err.println("lastch after EOF '" + lastch + "'");
			lastsym = TK_ERROR();
		}
		if (lastsym == TK_ERROR())
		{
			addNewError("Invalid character '" + lastch + "'");
		}
	}

	/**
	 * Initialize the scanner. Function is called in open(), but after the file is actually opened. Function initializes
	 * internal tables and tries to read the first token.
	 */
	public void Init()
	{
		InitKeywords();
		InitAttributes();
		getchar();
	}

	/**
	 * Open: Open an input file. Parameters are passed to Preprocessor.
	 *
	 * @param fileName
	 *            File name.
	 * @return true if no errors.
	 */
	public boolean Open(String fileName)
	{
		this.fileName = fileName;
		readNum = 0;
		boolean result = preprocessor.Open(fileName);
		if (result && !initialized)
		{
			initialized = true;
			Init();
		}
		return result;
	}

	/**
	 * Test if last keyword is same as expected. No error messages. Rule name parameter is not used.
	 *
	 * @param rule
	 *            Unused, name of the rule that tested the last keyword.
	 * @param kw
	 *            The keyword to check the last keyword against.
	 * @return true if last keyword is correct.
	 */
	public boolean test(String rule, Keyword kw)
	{
		return lastsym == kw;
	}

	/**
	 * Test if last keyword is same as expected. No error messages. Rule name parameter is not used.
	 *
	 * @param rule
	 *            Unused, name of the rule that tested the last keyword.
	 * @param kw
	 *            The keyword to check the last keyword against.
	 * @param stop
	 *            Set of keywords to expect next, everything not in this set is discarded.
	 * @return true if last keyword is correct.
	 */
	public boolean test(String rule, Keyword kw, Set<Keyword> stop)
	{
		if (!test(rule, kw))
		{
			if (stop != null)
			{
				addNewError("In rule '" + rule + "' scanned '" + ToString(lastsym) + "' while expecting " + kw);
				skip(stop);
			}
			return false;
		}
		return true;
	}

	/**
	 * Test if last keyword is part of the specified set. No error messages. Rule name parameter is not used.
	 *
	 * @param rule
	 *            Unused, name of the rule that tested the last keyword.
	 * @param set
	 *            Set of keywords to check the last keyword against.
	 * @return true if last keyword is contained in the set.
	 */
	public boolean test(String rule, Set<Keyword> set)
	{
		return set.Contains(lastsym);
	}

	/**
	 * Test if last keyword is part of the specified set. If keyword is not in the set, the message is printed and all
	 * tokens not in the stop set are absorbed and discarded.
	 *
	 * @param rule
	 *            Name of the rule that tested the last keyword.
	 * @param set
	 *            Set of keywords to check the last keyword against.
	 * @param stop
	 *            Set of keywords to expect next, everything not in this set is discarded.
	 * @return true if last keyword is contained in the set.
	 */
	public boolean test(String rule, Set<Keyword> set, Set<Keyword> stop)
	{
		if (!set.Contains(lastsym))
		{
			if (stop != null)
			{
				addNewError("In rule '" + rule + "' scanned '" + ToString(lastsym) + "' while expecting "
						+ set.toString(this));
				skip(stop);
			}
			return false;
		}
		return true;
	}

	public Keyword TK_CCONST()
	{
		return null;
	}

	public abstract Keyword TK_EOF();

	public abstract Keyword TK_ERROR();

	public Keyword TK_FCONST()
	{
		return null;
	}

	public Keyword TK_ICONST()
	{
		return null;
	}

	public Keyword TK_ID()
	{
		return null;
	}

	public Keyword TK_SCONST()
	{
		return null;
	}

	public String ToString(Keyword symbol)
	{
		String string = Lookup(symbol);
		if (string != null)
		{
			return string;
		}
		if (symbol == TK_CCONST())
		{
			return "<Char>";
		}
		if (symbol == TK_EOF())
		{
			return "<EOF>";
		}
		if (symbol == TK_ID())
		{
			return "<Identifier>";
		}
		if (symbol == TK_ERROR())
		{
			return "<Error>";
		}
		if (symbol == TK_SCONST())
		{
			return "<String>";
		}
		if (symbol == TK_ICONST())
		{
			return "<Integer>";
		}
		if (symbol == TK_FCONST())
		{
			return "<Float>";
		}
		return "<Unknown>";
	}

	/**
	 * Creates the location object
	 *
	 * @return
	 */
	Location createLocation()
	{
		return new Location(fileName, lineNum, linePos);
	}

	protected void AddKeyword(Keyword kw)
	{
		keywords.put(kw.toString(), kw);
	}

	// TODO add skip space handling
	protected void getchar()
	{
		if (lastLine.length() == 0)
		{
			if (lastch != '\n')
			{
				lastch = '\n';
				return;
			}
			else
			{
				lastLine = preprocessor.readLine();
				lastFullLine = lastLine;
				if (lastLine == null)
				{
					lastch = (char) -1;
					lastsym = TK_EOF();
					return;
				}
				if (lastLine.length() == 0)
				{
					lastch = '\n';
					return;
				}
			}
		}
		lastch = lastLine.charAt(0);
		lastLine = lastLine.substring(1);
		readPos++;
	}

	/**
	 * Get an identifier from input stream. Function is called when starting character of the identifier is read, so
	 * this function reads the rest. lastsym is set to token code of the keyword or tk_id if identifier is not found in
	 * the keyword table. lastid is set to the sequence of characters in the keyword or identifier.
	 */
	protected void GetIdentifier()
	{
		lastid = "";
		lastid += lastch;
		getchar();
		while (IsIdent2())
		{
			lastid += lastch;
			getchar();
		}
		lastsym = keywords.containsKey(lastid) ? keywords.get(lastid) : TK_ID();
	}

	/**
	 * Get a decimal integer constant. Basic scanner does not support floating point values. lastsym is set to
	 * tk_iconst. lastid is set to the sequence of decimal digit characters in the integer.
	 */
	protected void GetNumber()
	{
		lastid = "";
		lastid += lastch;
		getchar();
		if (lastid.equals("0") && (lastch == 'x' || lastch == 'X'))
		{
			lastid += lastch;
			getchar();
			while (IsXDigit())
			{
				lastid += lastch;
				getchar();
			}
		}
		while (IsDigit())
		{
			lastid += lastch;
			getchar();
		}
		lastsym = TK_ICONST();
		if (lastch == '.')
		{
			lastid += lastch;
			getchar();
			while (IsDigit())
			{
				lastid += lastch;
				getchar();
			}
			lastsym = TK_FCONST();
		}
		if (lastch == 'e' || lastch == 'E')
		{
			lastid += lastch;
			getchar();
			if (lastch == '+' || lastch == '-')
			{
				lastid += lastch;
				getchar();
			}
			while (IsDigit())
			{
				lastid += lastch;
				getchar();
			}
			lastsym = TK_FCONST();
		}
	}

	/**
	 * Get an operator from inout stream. Function reads all operator characters and then tries to match the longest
	 * sequence possible. lastsym is set to operator token code or tk_error. lastid is set to the sequence of characters
	 * in the operator.
	 */
	protected void GetOperator()
	{
		lastid = "";
		lastid += lastch;
		getchar();
		while (IsOperator())
		{
			lastid += lastch;
			getchar();
		}
		while (lastid.length() > 0)
		{
			lastsym = keywords.get(lastid);
			if (lastsym != null)
			{
				return;
			}
			unget();
		}
		lastsym = TK_ERROR();
	}

	/**
	 * Get a string from input stream. Opening delimiter is already read so it can be stored to use as a closing
	 * delimiter. This function supports basic character and string constants, anything more specific must be handled in
	 * derived scanners.
	 */
	protected void GetString()
	{
		char delimiter = lastch;
		lastid = "";
		getchar();
		while (lastch != delimiter)
		{
			if (lastch == '\\')
			{
				lastid += lastch;
				getchar();
			}
			lastid += lastch;
			getchar();
		}
		getchar();
		if (delimiter == '\'')
		{
			lastsym = TK_CCONST();
		}
		else
		{
			lastsym = TK_SCONST();
		}
	}

	protected abstract void InitAttributes();

	protected abstract void InitKeywords();

	/**
	 * Simplistic check for string delimiter. Default scanner implementation is capable of handling strings delimited by
	 * single or double quotes.
	 *
	 * @return boolean True if lastch is a quote of some kind.
	 */
	protected boolean IsDelimiter()
	{
		return lastch == '\'' || lastch == '\"';
	}

	protected boolean IsDigit()
	{
		return "0123456789".indexOf(lastch) >= 0;
	}

	protected boolean IsIdent()
	{
		return "_ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".indexOf(lastch) >= 0;
	}

	protected boolean IsIdent2()
	{
		if (IsIdent())
		{
			return true;
		}
		return IsDigit();
	}

	protected boolean IsOperator()
	{
		return "@!%#^&*()-+={}[]|:;<>?,.$/~".indexOf(lastch) >= 0;
	}

	protected boolean IsSpace()
	{
		return "\r\n\t\b ".indexOf(lastch) >= 0;
	}

	protected boolean IsXDigit()
	{
		return "0123456789ABCDEFabcdef".indexOf(lastch) >= 0;
	}

	protected String Lookup(Keyword kw)
	{
		for (String s : keywords.keySet())
		{
			if (keywords.get(s) == kw)
			{
				return s;
			}
		}
		return null;
	}

	protected void SetAttributes(String string, int attr)
	{
		for (int i = 0; i < string.length(); i++)
		{
			String ch = "";
			ch += string.charAt(i);
			Integer attrib = chtable.get(ch);
			attrib = attrib == null ? attr : attrib | attr;
			chtable.put(ch, attrib);
		}
	}

	private void addNewError(String message)
	{
		errors.add(new CompileError(message, createLocation()));
	}

	/**
	 * Skip all symbols that are not in the stop set.
	 *
	 * @param stop
	 *            Set of symbols to discard.
	 */
	private void skip(Set<Keyword> stop)
	{
		while (!stop.Contains(lastsym))
		{
			getsym();
		}
	}

	// unget the lastch
	private void unget()
	{
		if (lastch != '\n')
		{
			lastLine = lastch + lastLine;
			readPos--;
		}
		lastch = lastid.charAt(lastid.length() - 1);
		lastid = lastid.substring(0, lastid.length() - 1);
	}
}
