package base;

import java.io.*;
import java.util.*;

public class Set<Keyword>
{
	private Map<Keyword, Keyword> keywords = new HashMap<>();

	public boolean Contains(Keyword kw)
	{
		return keywords.get(kw) != null;
	}

	public void init(Keyword[] keywords)
	{
		for (Keyword kw : keywords)
		{
			this.keywords.put(kw, kw);
		}
	}

	public void print(PrintStream stream, BaseScanner<Keyword> scanner)
	{
		stream.print(toString(scanner));
	}

	public String toString(BaseScanner<Keyword> scanner)
	{
		StringBuilder str = new StringBuilder(1000);
		boolean first = true;
		for (Keyword kw : keywords.keySet())
		{
			if (first)
			{
				first = false;
			}
			else
			{
				str.append(", ");
			}
			str.append(scanner.ToString(kw));
		}
		return str.toString();
	}
}
